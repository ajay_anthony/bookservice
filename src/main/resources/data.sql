DROP TABLE IF EXISTS authors;

CREATE TABLE authors (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL
);

INSERT INTO authors (name) VALUES
  ('Rowling'),
  ('Bill Gates'),
  ('Stephen Fry');