package com.epam.ajay.bookservice.model;

public enum Category {
    KIDS,
    SCI_FI,
    TECHNICAL,
    SELF_IMPROVEMENT
}
